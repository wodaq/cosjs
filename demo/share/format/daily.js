//日数据
exports = module.exports = {
    "shop":{'type':'json','val':{}},                          //商城礼包记录
    "payweal":{'type':'json','val':{}},                       //月卡福利记录
    "dayVipExp":{'type':'int','val':0},                        //连续登录送vip
    "dayTaskLog":{'type':'json',val:{}},                      //每日任务领取记录
    "battleField":{'type':'json',val:{}},                     //战场每日记录{honor:今日获得荣誉}
    "freeDraw":{'type':'json',val:{}},                        //免费抽奖{type:num}
};
